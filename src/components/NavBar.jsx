import React from 'react'

const NavBar = () => {
  return (
    <div className='h-16 bg-slate-700 flex p-3'>
        <h1 className='text-white text-4xl px-4 '>NavBar</h1>
        </div>
  )
}

export default NavBar;